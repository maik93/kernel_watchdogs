#ifndef __EX_DEV_PING_H__
#define __EX_DEV_PING_H__

#define PING_DEV_NAME "watchdog_ping"

int ping_device_create(void);

void ping_device_destroy(void);

#endif /* __EX_DEV_PING_H__ */
