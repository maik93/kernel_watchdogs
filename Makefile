# desired module name
MODULE_NAME = watchdog_module

# all source names (both main and its dependencies) without extensions, separated by spaces
SOURCES = main dev_ping dev_trap thread_watchdog

TEST_SCRIPT_FILE = test.sh

# system kernel build
#KERNEL_DIR ?= /lib/modules/`uname -r`/build

# local kernel build
KERNEL_DIR ?= ../linux-5.10.16

# main object, used by kbuild to produce the final MODULE_NAME.ko file
obj-m = $(MODULE_NAME).o

# source dependencies in the kbuild process
#$(MODULE_NAME)-objs := $(shell echo $(SOURCES) | sed '/^$$/!s/\>/\.o/g')
# or simply...
$(MODULE_NAME)-objs := $(addsuffix .o, $(SOURCES))

# substitute any dot with an underscore in test script name
TEST_SCRIPT_GZ = $(subst .,_,$(TEST_SCRIPT_FILE))

all:
	make -C $(KERNEL_DIR) M=`pwd` modules

clients:
	make -C user_nodes pack

pack: all clients
	# pack the compiled module in a gunzip
	echo $(MODULE_NAME).ko | cpio -H newc -o | gzip > tmp_$(MODULE_NAME).gz
	# pack the test script too
	echo $(TEST_SCRIPT_FILE) | cpio -H newc -o | gzip > tmp_$(TEST_SCRIPT_GZ).gz
	# add it to the minimal ramdisk for qemu
	cat tmp_$(MODULE_NAME).gz tmp_$(TEST_SCRIPT_GZ).gz user_nodes/tmp_clients.gz tinyfs.gz > tinyfs_$(MODULE_NAME).gz

run: pack
	qemu-system-x86_64 -kernel $(KERNEL_DIR)/arch/x86/boot/bzImage -initrd tinyfs_$(MODULE_NAME).gz

clean:
	make -C $(KERNEL_DIR) M=`pwd` clean
	make -C user_nodes clean
	rm -f tmp_$(MODULE_NAME).gz tmp_$(TEST_SCRIPT_GZ).gz tinyfs_$(MODULE_NAME).gz
