#ifndef __EX_DEV_TRAP_H__
#define __EX_DEV_TRAP_H__

#define TRAP_DEV_NAME "watchdog_trap"

extern bool killer_node_connected;

int trap_device_create(void);

void trap_device_destroy(void);

#endif /* __EX_DEV_TRAP_H__ */
