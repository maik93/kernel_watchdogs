#include <linux/poll.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/miscdevice.h>

#include "thread_watchdog.h"
#include "dev_trap.h"

/// Either a user node developed to kill monitored nodes is connected or vacant.
bool killer_node_connected;

/**
 * Misc. device opening: accept connections only if there is no killer node connected; refuse to open otherwise.
 */
static int trap_device_open(struct inode *inode, struct file *file) {
    if (!killer_node_connected) {
        killer_node_connected = true;
        return 0;
    } else {
        printk("Trying to connect a node, but already one is connected!");
        return -1;
    }
}

/**
 * Clear the list of monitored tasks on killer node disconnection.
 */
static int trap_device_close(struct inode *inode, struct file *file) {
    clear_monitored_pids(); // ensure that the list is empty when the killer node is disconnected
    killer_node_connected = false;
    return 0;
}

/**
 * Lock the reading task until `release_killer` is completed (i.e. a fault occurred on the monitored tasks), then give back
 * the list of monitored PIDs (that usually needs to be killed) by popping them out from the list (thus removing any entry).
 * @param usr_buff the read buffer that will be populated with a '_'-separated sequence of monitored PIDs (e.g. '123_4567')
 */
ssize_t trap_device_read(struct file *file, char __user *usr_buff, size_t len, loff_t *ppos) {
    int completion_return;
    pid_t popped_pid;

    // trigger the trap: anyone how read will stop here until a complete signal arrives
    completion_return = wait_for_completion_interruptible(&release_killer);

    // check if an interrupt has occurred
    if (completion_return == ERESTARTSYS) {
        return -1;
    }

    popped_pid = pop_pid_to_be_killed();
    if (popped_pid != 0) {
#ifdef DEBUGGING_HARD
        printk("Writing %d in usr_buff", popped_pid);
#endif
        // FIXME: why a trailing space cannot be printed?
        sprintf(usr_buff, "%d_", popped_pid); // the trailing symbol let other PIDs concatenate
        return strlen(usr_buff);
    } else {
#ifdef DEBUGGING_HARD
        printk("No more PIDs to read!");
#endif
        return 0;
    }
}

/**
 * Trap device can be only read, no write function implemented.
 */
static struct file_operations thread_fops = {
        .owner =        THIS_MODULE,
        .open =         trap_device_open,
        .read =         trap_device_read,
        .release =      trap_device_close,
};

static struct miscdevice misc_device_trap = {
        MISC_DYNAMIC_MINOR, TRAP_DEV_NAME, &thread_fops
};

int trap_device_create(void) {
    killer_node_connected = false;
    return misc_register(&misc_device_trap);
}

void trap_device_destroy(void) {
    misc_deregister(&misc_device_trap);
}
