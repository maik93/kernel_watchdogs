#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kthread.h>
#include <linux/mutex.h>
#include <linux/completion.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/list.h>

#include "thread_watchdog.h"
#include "dev_trap.h"

/// ID of the kernel thread.
static struct task_struct *thread_id;

/// Completion that unlocks the read function of trap device
struct completion release_killer; // not static because exported in the header

// ---------- list of user tasks to control -----------
/// List of monitored task (user nodes that have pinged at least once) that the watchdog is monitoring.
static struct list_head list_of_pids;
static struct mutex list_mutex;

struct node {
    struct list_head header; // mandatory, it allows to concatenate elements on a list
    /// The Process Identifier of the user program that have to be monitored.
    pid_t pid;
    /// Either the monitored task has pinged during the last watchdog cycle or not.
    bool has_pinged;
};

/**
 * Clean up the list of monitored tasks.
 */
void clear_monitored_pids(void) {
    struct list_head *loop_cursor, *tmp;
    struct node *n;

    printk("Cleaning up list...\n");
    mutex_lock(&list_mutex);
    list_for_each_safe(loop_cursor, tmp, &list_of_pids) { // macro that iterates on the list, while allowing deletions (safe)
        n = list_entry(loop_cursor, struct node, header); // retrieve the node element from the list_head
        printk("-> PID %d removed\n", n->pid);
        list_del(loop_cursor);
        kfree(n);
    }
    mutex_unlock(&list_mutex);
}

/**
 * Retrieve the PID of the first element of the monitored task list and remove its entry.
 * @return the retrieved PID.
 */
pid_t pop_pid_to_be_killed(void) {
    struct node *n;
    pid_t extracted_pid;

    if (list_empty(&list_of_pids)) {
        return 0;
    }

    // retrieve the PID of the first element of the list
    mutex_lock(&list_mutex);
    n = list_entry(list_of_pids.next, struct node, header);
    extracted_pid = n->pid;
#ifdef DEBUGGING
    printk("Popping PID %d", extracted_pid);
#endif

    // then remove the entry
    list_of_pids = *list_of_pids.next;
    kfree(n);
    mutex_unlock(&list_mutex);

    return extracted_pid;
}

/**
 * Print in the kernel log the list of monitored PIDs, together with they 'has_pinged' flag.
 */
void print_monitored_pids(void) {
    struct list_head *loop_cursor;
    struct node *n;
    int c = 0;

    printk("List of monitored PIDs:\n");
    mutex_lock(&list_mutex);
    list_for_each(loop_cursor, &list_of_pids) {
        n = list_entry(loop_cursor, struct node, header);
        printk("-> PID %d - has_pinged: %d\n", n->pid, n->has_pinged);
        c++;
    }
    mutex_unlock(&list_mutex);
    printk("-> %d PIDs in total to be monitored.\n", c);
}
// ----------------------------------------------------

// ----- ping update logic (handling new inserts) -----
/**
 * Insert a new element on top of the monitored PID list. Flag 'has_pinged' as true, since the task has just pinged.
 * @param pid the PID to be inserted.
 */
void insert_pid_to_monitor(pid_t pid) {
    struct node *n = kmalloc(sizeof(struct node), GFP_KERNEL); // memory not shared with user space
    n->pid = pid;
    n->has_pinged = true;

    mutex_lock(&list_mutex);
    list_add(&(n->header), &list_of_pids); // insert on top
    mutex_unlock(&list_mutex);
}

/**
 * Search for the given PID in the list of monitored tasks; if found, switch on its 'has_pinged' flag, insert a new
 * element otherwise.
 * @param pid the PID to be flagged as pinged (inserted if not found, updated otherwise).
 */
void update_ping(pid_t pid) {
    struct list_head *loop_cursor;
    struct node *n;

    // do not store anything if there's no killer connected
    if (!killer_node_connected) return;

    // search for the incoming PID
    mutex_lock(&list_mutex);
    list_for_each(loop_cursor, &list_of_pids) {
        n = list_entry(loop_cursor, struct node, header);
        if (n->pid == pid) {
            // process found in the list, update ping signal
            n->has_pinged = true;

            mutex_unlock(&list_mutex);
            return;
        }
    }
    mutex_unlock(&list_mutex);

    // if we are here, the given PID is not present in the list, so it must be inserted
    insert_pid_to_monitor(pid);
}
// ----------------------------------------------------

/**
 * The kernel thread that checks if all the monitored tasks has pinged during the last cycle; if so, it resets their
 * flags and sleep until the next timeout. If at least one monitored task has not pinged, it releases the killer task.
 */
static int watchdog_thread(void *arg) {
    struct list_head *loop_cursor;
    struct node *n;
    bool is_everything_fine;

    while (!kthread_should_stop()) { // this separation allows consecutive monitoring without reloading the module
        is_everything_fine = true;

        while (is_everything_fine && !kthread_should_stop()) {
            mutex_lock(&list_mutex);
            list_for_each(loop_cursor, &list_of_pids) {
                n = list_entry(loop_cursor, struct node, header);

                if (n->has_pinged) {
#ifdef DEBUGGING_HARD
                    printk("PID %d has correctly pinged during the last cycle.\n", n->pid);
#endif
                    n->has_pinged = false; // reset for the next cycle
                } else {
                    printk("PID %d has not pinged in %dms, releasing the killer!\n", n->pid, ttl);
                    is_everything_fine = false;
                    break;
                }
            }
            mutex_unlock(&list_mutex);

            msleep(ttl); // milliseconds
        }

        // release the user killer node, this will pop PIDs to kill from the list
        while (killer_node_connected && !kthread_should_stop()) {
            // let the list flush through the trap device
            complete(&release_killer);
            msleep(5);
        }
    }

#ifdef DEBUGGING_HARD
    printk("Thread closed.\n");
#endif
    mutex_destroy(&list_mutex);
    printk("Kernel thread closed!\n");
    return 0;
}

int thread_create(void) {
    mutex_init(&list_mutex);
    init_completion(&release_killer);
    INIT_LIST_HEAD(&list_of_pids); // macro that initialises the list

    thread_id = kthread_run(watchdog_thread, NULL, "watchdog_thread");
    if (IS_ERR(thread_id)) {
        printk("Error creating kernel thread!\n");

        return (int) PTR_ERR(thread_id);
    }

    return 0;
}

void thread_destroy(void) {
    clear_monitored_pids();
    kthread_stop(thread_id);
}
