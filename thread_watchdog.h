#ifndef __THREAD_WATCHDOG_H__
#define __THREAD_WATCHDOG_H__

#define DEBUGGING
//#define DEBUGGING_HARD

extern int ttl;

extern struct completion release_killer;

int thread_create(void);

void thread_destroy(void);

void update_ping(pid_t pid);

void print_monitored_pids(void);

void clear_monitored_pids(void);

pid_t pop_pid_to_be_killed(void);

#endif    /* __THREAD_WATCHDOG_H__ */
