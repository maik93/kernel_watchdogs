#!/usr/bin/env sh

echo "Spawning killer node..."
sudo /node_that_kill &

sleep 0.1
echo "Spawning some nice clients..."
sudo /node_to_monitor 100 &
sudo /node_to_monitor 120 &

echo "Spawning a faulty client..."
sudo /node_to_monitor 90 5 1 &

echo "User nodes setup done!"
