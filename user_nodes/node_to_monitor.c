#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <signal.h>
#include <unistd.h>

#define DEV_PING "/dev/watchdog_ping"
//#define DEBUGGING

#define DEFAULT_N_GOOD_ITERATIONS 25
#define N_FAULTY_ITERATIONS 25
#define FAULTY_SLEEP_MULTIPLIER 3

pid_t this_pid;

/**
 * Check argument consistency and parse them.
 * @param refresh_period_ms sleep time for each iteration.
 * @param n_good_iterations number of iteration that needs to be done with period `refresh_period_ms`.
 * @param faulty_node either to be a faulty node: if so, sleep iteration raises to `FAULTY_SLEEP_MULTIPLIER * refresh_period_ms`
 * after `n_good_iterations` has been done. This faulty iterations will be N_FAULTY_ITERATIONS in total.
 */
void parse_input(int argc, char **argv, long *refresh_period_ms, int *n_good_iterations, int *faulty_node) {
    char *end_ptr;

    // check argument number consistency
    if (argc < 2 || argc > 4) {
        fprintf(stderr, "Incorrect usage. Expected '%s refresh_period_ms [n_good_iterations] [is_faulty]'\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    // collect the refresh rate
    *refresh_period_ms = strtol(argv[1], &end_ptr, 10);
    if (*refresh_period_ms <= 0 || end_ptr[0] != '\0') {
        fprintf(stderr, "Strange refresh_period_ms... are you sure it is a positive integer, in milliseconds?\n");
        exit(EXIT_FAILURE);
    }

    // check for optional arguments
    if (argc == 2) {
        *n_good_iterations = DEFAULT_N_GOOD_ITERATIONS;
        *faulty_node = 0;
        return;
    }

    // check the number of good iterations
    *n_good_iterations = (int) strtol(argv[2], &end_ptr, 10);
    if (*n_good_iterations <= 0 || end_ptr[0] != '\0') {
        fprintf(stderr, "Strange n_good_iterations... It should be a positive integer.\n");
        exit(EXIT_FAILURE);
    }

    // check if this node should be faulty
    if (argc == 4) {
        *faulty_node = (int) strtol(argv[3], &end_ptr, 10);
        if (!(*faulty_node == 0 || *faulty_node == 1) || end_ptr[0] != '\0') {
            fprintf(stderr, "Strange faulty_node... It should be a boolean (either 0 or 1).\n");
            exit(EXIT_FAILURE);
        }
    }
}

/**
 * Handle SIGTERM signals, adding a print before the node exit.
 */
void sig_term_handler(int signum, siginfo_t *info, void *ptr) {
    printf("[user node %d] Ehi, I've been killed!\n", this_pid);
    exit(0);
}

/**
 * Register `sig_term_handler` as callback function for SIGTERM signals.
 */
void catch_sigterm() {
    static struct sigaction action;

    memset(&action, 0, sizeof(action));
    action.sa_sigaction = sig_term_handler;
    action.sa_flags = SA_SIGINFO;
    sigaction(SIGTERM, &action, NULL);
}

/**
 * Periodic actions of the node: it simply writes on the ping device every `refresh_period_ms`.
 * @param dev the device file in witch to write.
 * @param n_iterations now many iterations should be done.
 * @param refresh_period_ms the sleeping period between writes.
 */
void ping_cycle(FILE *dev, int n_iterations, long refresh_period_ms) {
    int i;

    for (i = 0; i < n_iterations; ++i) {
        fprintf(dev, "ehi!\n");
        fflush(dev); // IMPORTANT! Flush the buffers, otherwise the device is not actually written TODO: maybe read instead?
#ifdef DEBUGGING
        printf("[user node %d] Pinged!\n", this_pid);
#endif
        usleep(refresh_period_ms * 1000);
    }
}

/**
 * A node that iteratively writes on the ping device with the given period, then slows down if `faulty_node` is enabled.
 */
int main(int argc, char **argv) {
    int n_good_iterations, faulty_node;
    long refresh_period_ms;
    FILE *dev_ping;

    // check root privileges
    if (getuid() != 0) {
        printf("This program needs root privileges. Closing.\n"); // TODO: set a udev rule for /dev/watchdog_* instead
        exit(1);
    }

    catch_sigterm();

    parse_input(argc, argv, &refresh_period_ms, &n_good_iterations, &faulty_node);
    dev_ping = fopen(DEV_PING, "w");

    this_pid = getpid();

    printf("[user node %d] Starting to spin at %ld ms (%d iterations)\n", this_pid, refresh_period_ms, n_good_iterations);
    ping_cycle(dev_ping, n_good_iterations, refresh_period_ms);

    if (faulty_node) {
        printf("[user node %d] Switching to slower spin rate: %ld ms (%d iterations)\n",
               this_pid, refresh_period_ms * FAULTY_SLEEP_MULTIPLIER, N_FAULTY_ITERATIONS);
        ping_cycle(dev_ping, N_FAULTY_ITERATIONS, refresh_period_ms * FAULTY_SLEEP_MULTIPLIER);
    }

    printf("[user node %d] Final iteration reached, goodbye!\n", this_pid);

    fclose(dev_ping);
    return EXIT_SUCCESS;
}
