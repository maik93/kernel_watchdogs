#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define DEV_TRAP "/dev/watchdog_trap"
//#define DEBUGGING

/// programs can be just killed nicely (SIGTERM) or killed with a SIGKILL if they do not quit in NASTY_KILL_TIMEOUT_MS milliseconds.
#define NASTY_KILL
#define NASTY_KILL_TIMEOUT_MS 100

/**
 * Convert the string read from watchdog_trap (e.g. '1234_1234_') in a space-separated string (e.g. "1234 1234 ").
 * @param str the string that needs to be digested (will be modified).
 */
void digest_pid_string(char *str) {
    char *current_pos;

    for (current_pos = strchr(str, '_'); (current_pos = strchr(current_pos, '_')) != NULL; *current_pos = ' ');
}

/**
 * At start, this node locks on a read action on the trap device. When released, it simply execute a 'kill' command
 * over the list of PIDs that has return from the read action.
 */
int main() {
    FILE *dev_trap;
    char buff[255], cmd[260];

    // check root privileges
    if (getuid() != 0) {
        printf("This program needs root privileges. Closing.\n"); // TODO: set a udev rule for /dev/watchdog_* instead
        exit(1);
    }

    dev_trap = fopen(DEV_TRAP, "r");
    if (dev_trap == NULL) {
        fprintf(stderr, "[killer_node] Error while opening misc device!\n");
        return EXIT_FAILURE;
    }

    // read something in the trap; this should lock this program
    fscanf(dev_trap, "%s", buff);
    fclose(dev_trap);

    printf("[killer_node] Killer program unlocked!\n");
#ifdef DEBUGGING
    printf("[killer_node] got %lu characters: %s\n", strlen(buff), buff);
#endif

    // develop and execute the 'kill' command
    digest_pid_string(buff);
#ifdef DEBUGGING
    printf("Digested: %s\n", buff);
#endif
    sprintf(cmd, "kill %s", buff);
#ifdef NASTY_KILL
    printf("[killer_node] Executing '%s --timeout %d KILL'\n", cmd, NASTY_KILL_TIMEOUT_MS);
#else
    printf("[killer_node] Executing '%s'\n", cmd);
#endif
    system(cmd);

    return EXIT_SUCCESS;
}
