#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include "thread_watchdog.h"
#include "dev_ping.h"
#include "dev_trap.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Michael Mugnai");

MODULE_DESCRIPTION("Linux Kernel Watchdogs for user nodes.");
MODULE_VERSION("0.1");

/// watchdog refresh time in milliseconds.
int ttl = 2000;

module_param(ttl, int, 0);// can be specified when the module is loaded - default 2000ms

/**
 * Instantiate the kernel thread and two misc devices: watchdog_ping (for user periodic node monitoring)
 * and watchdog_trap (lock interface for the user node developed to kill monitored nodes).
 */
static int __init watchdog_init(void) {
    int res;

    res = thread_create();
    if (res < 0) {
        return res;
    }

    res = trap_device_create();
    if (res != 0) {
        printk("Error while registering trap device!\n");
        thread_destroy();

        return res;
    }

    res = ping_device_create();
    if (res != 0) {
        printk("Error while registering ping device!\n");
        thread_destroy();

        return res;
    }

    printk("Module successfully loaded with refresh time %dms.\n", ttl);
    return 0;
}

static void __exit watchdog_cleanup(void) {
    ping_device_destroy();
    trap_device_destroy();
    thread_destroy();
}

module_init(watchdog_init)

module_exit(watchdog_cleanup)
