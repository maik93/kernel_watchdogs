#include <linux/poll.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/miscdevice.h>

#include "thread_watchdog.h"
#include "dev_trap.h"
#include "dev_ping.h"

/**
 * Misc. device opening: refuse to open if there is no killer node connected.
 */
static int ping_device_open(struct inode *inode, struct file *file) {
    // allow connections only if the killer node is already connected
    if (killer_node_connected) {
        return 0;
    } else {
        printk("Trying to ping the watchdog device, but no killer node connected!");
        return -1;
    }
}

static int ping_device_close(struct inode *inode, struct file *file) {
    return 0;
}

/**
 * Any write action collects the PID of the writing task and updates the list of monitored tasks.
 */
static ssize_t ping_device_write(struct file *file, const char __user *buf, size_t size, loff_t *ppos) {
    // global ID, i.e. the id seen from the init namespace (ref: https://stackoverflow.com/a/15541638/3158286)
    pid_t pid = task_pid_nr(current);
    update_ping(pid);
#ifdef DEBUGGING
    printk("Ping acquired. User node PID: %d\n", pid);
#endif
#ifdef DEBUGGING_HARD
    print_monitored_pids();
#endif

    return size;
}

/**
 * Ping device can be only written, no read function implemented.
 */
static struct file_operations thread_fops = {
        .owner =        THIS_MODULE,
        .open =         ping_device_open,
        .write =        ping_device_write,
        .release =      ping_device_close,
};

static struct miscdevice misc_device_ping = {
        MISC_DYNAMIC_MINOR, PING_DEV_NAME, &thread_fops
};

int ping_device_create(void) {
    return misc_register(&misc_device_ping);
}

void ping_device_destroy(void) {
    misc_deregister(&misc_device_ping);
}
